const app = new Vue({
    el: "#app",
    data() {
        return {
            tables: tables,
            activeTable: null,
            products: products,
            active: true,
            discount: 0,
            detailsTemp: [],
            auth: auth
        };
    },
    mounted() {
        console.log(this.tables);
    },
    methods: {
        getTable: function(row, col) {
            const table = this.tables.find(
                el => el.row == row && el.col == col
            );
            return table ?? null;
        },
        switchTable: function(row, col) {
            if (this.getTable(row, col).active > 0) {
                if (auth == null) {
                    return alert("กรุณาเข้าสู่ระบบก่อนค่ะ");
                }

                if (!this.getTable(row, col).sale_order.status) {
                    this.activeTable = JSON.parse(
                        JSON.stringify(this.getTable(row, col))
                    );
                    if (this.activeTable.sale_order.id == null) {
                        this.activeTable.sale_order = {
                            id: null,
                            details: [],
                            discount: 0
                        };
                    }
                } else {
                    alert("โต๊ะดังกล่าวไม่สามารถจองได้ค่ะ");
                }
            }
        },
        getTableStatus: function() {
            return !(this.activeTable.sale_order.status == null);
        },
        toBath: function(number) {
            return number.toLocaleString("th-TH", {
                style: "currency",
                currency: "BTH"
            });
        },
        getProduct: function(id) {
            return { ...this.products.find(e => e.id == id) };
        },
        remove: function(detail) {
            const index = this.activeTable.sale_order.details.findIndex(
                e => e.id == detail.id
            );
            if (index >= 0) {
                this.activeTable.sale_order.details.splice(index, 1);
            }
        },
        add: function() {
            this.activeTable.sale_order.details.push({
                product_id: this.products[0]["id"],
                quantity: 1
            });
        }
    },
    computed: {
        sumActiveTable() {
            let sum = 0;
            if (
                this.activeTable.sale_order.details != undefined &&
                this.activeTable.sale_order.details.length > 0
            ) {
                for (const detail of this.activeTable.sale_order.details) {
                    sum +=
                        detail.quantity *
                        this.getProduct(detail.product_id).price;
                }
                sum -= this.activeTable.sale_order.discount;
            }
            return sum;
        }
    }
});
