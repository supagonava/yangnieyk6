Highcharts.chart("chart-container", {
    chart: {
        type: "column"
    },
    title: {
        text: "แผนภูมิยอดขาย"
    },
    subtitle: {
        text: ""
    },
    xAxis: {
        categories: categories,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: "บาท (bth)"
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat:
            '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:2px"> <b>{point.y} </b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: series,
});
