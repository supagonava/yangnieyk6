<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('theme/css/style.min.css') }}" rel="stylesheet">
    <style>
        #body {
            background-image: url('/images/bg2.jpg');
        }
    </style>
</head>

<body class="max-w-screen" id="body">
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div class="flex flex-col min-h-screen">
        <nav class="flex-none bg-yellow-200 py-3 text-yellow-700 h-24 md:h-16">
            <div class="flex flex-col md:flex-row justify-between container">
                <a href="{{ route('public.index') }}"
                    class="flex gap-2 items-center align-middle justify-center md:justify-start">
                    <img class="w-10 h-10 rounded-full ring-2 ring-yellow-400" src="{{ asset('images/logo.jpg') }}">
                    <p class="text-yellow-700 pt-1 text-xl font-bold"> {{ config('app.name') }}</p>
                </a>
                <div class="flex justify-center md:justify-end gap-1 md:gap-3">
                    <a href="{{ route('public.index') }}"
                        class="p-2 text-yellow-700 hover:bg-yellow-400 hover:text-yellow-700">
                        <i class="fas fa-home"></i> หน้าแรก
                    </a>
                    <a href="{{ route('public.how-to') }}" class="p-2 text-yellow-700 hover:bg-yellow-400 hover:text-yellow-700">
                        <i class="fas fa-home"></i> คุ่มือการใช้งาน
                    </a>
                    @auth
                    @if (Auth::user() && Auth::user()->role_id == 30)
                    <a class="p-2 text-yellow-700 hover:bg-yellow-400 hover:text-yellow-700"
                        href="{{ route('home.index') }}">
                        <i class="fas fa-toolbox"></i> หลังบ้าน
                    </a>

                    @endif
                    <div class="dropdown open">
                        <button class="p-2 text-yellow-700 hover:bg-yellow-400 hover:text-yellow-700" type="button"
                            id="userActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user"></i> {{ Auth::user()->name }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="userActions">
                            <button class="dropdown-item"><a href="{{ route('logout') }}">ออกจากระบบ</a></button>
                        </div>
                    </div>
                    @else
                    <a href="{{ route('login') }}" class="p-2 text-yellow-700 hover:bg-yellow-400 hover:text-yellow-700"
                        data-toggle="modal" data-target="#loginModal">
                        <i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ
                    </a>
                    @endauth
                </div>
            </div>
        </nav>

        <div class="flex-grow flex flex-col space-y-2 container h-full" id="app">
            @yield('content')
        </div>

        <footer class="flex-none footer bg-yellow-200 text-yellow-700 h-14">
            <div class="flex justify-end container">ย่างเนยคลองหก {{ date('Y') + 543 }}</div>
        </footer>
    </div>

    <script src="{{ asset('libs/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('extra-libs/sparkline/sparkline.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('theme/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('theme/js/sidebarmenu.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('theme/js/custom.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-dark">เข้าสู่ระบบ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid text-dark">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab"
                                    aria-controls="login" aria-selected="true">เข้าสู่ระบบ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab"
                                    aria-controls="register" aria-selected="false">ลงทะเบียน</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane p-2 fade show active" id="login" role="tabpanel"
                                aria-labelledby="login-tab">@include('auth.login')</div>
                            <div class="tab-pane p-2 fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                                @include('auth.register')</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @javascript('errors',$errors->all())
    @javascript('success',session('success'))
    <script src="{{ mix('js/app.js') }}"></script>
    <script>
        if(errors.length > 0){
            for (const message in errors) {
                alert(errors[message]);
            }
        }
        console.log(errors);
        if(success){
            alert(success);
        }
    </script>
    @yield('scripts')
</body>

</html>
