<header class="topbar" data-navbarbg="skin5">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <div class="navbar-header" data-logobg="skin5">

            <a class="navbar-brand flex justify-center" href="{{ route('home.index') }}">
                <h3>{{ config('app.name') }}</h3>
            </a>
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                    class="ti-menu ti-close"></i></a>
        </div>
        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
            <ul class="navbar-nav float-start me-auto">
            </ul>
            <ul class="navbar-nav float-end">
                <li class="nav-item dropdown">
                    <button class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href="#"
                        id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <label class="text-white text-md">{{ Auth::user()->name }}</label>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-end user-dd animated" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"><i class="fa fa-power-off me-1 ms-1"></i>
                            ออกจากระบบ</a>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
