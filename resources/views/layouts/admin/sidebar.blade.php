<aside class="left-sidebar" data-sidebarbg="skin5">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="pt-4">
                @foreach (App\SiteInfo::menus() as $item)
                    @if (empty($item['items']))
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link"
                                href="{{ $item['route'] ?? '#' }}" aria-expanded="false">
                                {!! $item['icon'] !!}
                                <span class="hide-menu pl-2">
                                    {{ $item['label'] }}
                                </span>
                            </a>
                        </li>
                    @else
                        <li class="sidebar-item">
                            <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
                                aria-expanded="false">
                                {!! $item['icon'] !!}
                                <span class="hide-menu pl-2">{{ $item['label'] }}
                                </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                @foreach ($item['items'] as $nestItem)
                                    <li class="sidebar-item ml-2 text-sm">
                                        <a href="{{ $nestItem['route'] ?? '#' }}"
                                            class="sidebar-link">
                                            {!! $nestItem['icon'] !!}
                                            <span class="hide-menu">{{ $nestItem['label'] }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach

                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-alert"></i><span class="hide-menu">Errors </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a href="error-403.html" class="sidebar-link"><i class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 403
                                </span></a></li>
                        <li class="sidebar-item"><a href="error-404.html" class="sidebar-link"><i class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 404
                                </span></a></li>
                        <li class="sidebar-item"><a href="error-405.html" class="sidebar-link"><i class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 405
                                </span></a></li>
                        <li class="sidebar-item"><a href="error-500.html" class="sidebar-link"><i class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 500
                                </span></a></li>
                    </ul>
                </li> --}}
            </ul>
        </nav>
    </div>
</aside>
