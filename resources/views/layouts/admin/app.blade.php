<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('theme/css/style.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/toastr/build/toastr.min.css') }}" rel="stylesheet">
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        @include('layouts.admin.nav')
        @include('layouts.admin.sidebar')
        <div class="page-wrapper" id="app">
            <div class="flex flex-col space-y-2 p-2 sm:p-8 min-h-screen">
                @include('admin.components.breadcrumb')
                <div class="p-2">
                    @yield('content')
                </div>
            </div>

            <footer class="footer text-center bg-black text-white">
                ย่างเนยคลองหก {{ date('Y') + 543 }}
            </footer>

        </div>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ asset('libs/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('extra-libs/sparkline/sparkline.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('theme/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('theme/js/sidebarmenu.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('theme/js/custom.min.js') }}"></script>
    <!-- this page js -->
    <script src="{{ asset('libs/toastr/build/toastr.min.js') }}"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <script>
        @if (session('success'))
            toastr.success("{{ session('success') }}", 'แจ้งเตือน!');
        @endif

        @if (session('errors'))
            @foreach (session('errors') as $error)
                toastr.error('{{ $error }}', 'แจ้งเตือน!');
            @endforeach
        @endif
    </script>
    @yield('scripts')
</body>

</html>
