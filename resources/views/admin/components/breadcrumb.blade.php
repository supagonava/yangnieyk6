<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h3 class="page-title w-1/2">{{ $title ?? Route::currentRouteName() }}</h3>
            {{-- <div class="ms-auto w-1/2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb flex justify-end">
                        <li class="breadcrumb-item text-base"><a href="{{ route('home.index') }}">หน้าหลัก</a></li>
                        @if (isset($breadcrumbs) && $breadcrumbs)
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        @endif
                    </ol>
                </nav>
            </div> --}}
        </div>
    </div>
</div>
