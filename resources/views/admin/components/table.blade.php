<div class="card">
    @if(!isset($show_add))
    <div class="flex items-center align-middle justify-between mb-2 card-header">
        <h5>{{ $card_title }}</h5>
        <a href="{{ route($create) }}" class="btn btn-success text-white">
            เพิ่ม <i class="fas fa-plus"></i>
        </a>
    </div>
    @endif
    <div class="card-body  overflow-x-auto max-w-full">
        <table class="table p-1">
            <thead class="thead-inverse">
                <tr class="text-center">
                    @foreach ($columns as $item)
                    <th>{{ $item }}</th>
                    @endforeach
                    @if (!isset($notShowAction))
                    <th>การกระทำ</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @forelse ($models as $model)
                <tr>
                    @foreach ($attributes as $atr)
                    <td>
                        @if (is_array($atr))
                        <img src="{{ $model->photo }}" class="w-40 h-40 object-contain" />
                        @else
                        {!! $model->$atr ?? 'ไม่ระบุ' !!}
                        @endif
                    </td>
                    @endforeach
                    @if (empty($notShowAction))
                    <td>
                        <div class="flex gap-1 justify-end">
                            <a class="btn btn-info" href="{{ route($show, [$param => $model->id]) }}"><i
                                    class="fa fa-eye" aria-hidden="true"></i></a>
                            @if(!isset($notEdit) )
                            <a class="btn btn-warning" href="{{ route($edit, [$param => $model->id]) }}"><i
                                    class="fa fa-edit" aria-hidden="true"></i></a>
                            @endif
                            <form action="{{ route($delete, [$param => $model->id]) }}"
                                onsubmit="return confirm('คุณต้องการจะลบรายการดังกล่าวหรือไม่?')" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger text-white"><i class="fas fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                    @endif
                </tr>
                @empty
                <tr>
                    <td colspan="{{ sizeOf($columns ?? []) + 1 }}" class="text-center p-2">
                        รายการว่างเปล่า
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        {!! $models->appends($_GET)->links() !!}
    </div>
</div>
