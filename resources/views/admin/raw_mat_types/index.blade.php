@extends('layouts.admin.app')
@section('content')
    @php
    $columns = ['ชื่อ', 'รายละเอียด'];
    $attributes = ['name', 'detail'];
    $options = ['data' => $models, 'card_title' => 'ตารางหมวดหมู่วัตถุดิบ', 'create' => 'raw-mat-type.create', 'show' => 'raw-mat-type.show', 'edit' => 'raw-mat-type.edit', 'delete' => 'raw-mat-type.destroy', 'param' => 'raw_mat_type', 'columns' => $columns, 'attributes' => $attributes];
    @endphp
    @include('admin.components.table', $options)
@endsection
