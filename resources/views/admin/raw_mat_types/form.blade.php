@extends('layouts.admin.app')

@section('content')
    @if ($mode == 'show')
        <div class="card">
            <div class="card-header flex justify-between items-center">
                <label class="text-base align-middle">การกระทำ</label>
                <div class="flex gap-1 justify-end">
                    <a class="btn btn-info" href="{{ route('raw-mat-type.edit', ['raw_mat_type' => $model->id]) }}">
                        <i class="fas fa-edit"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th scope="row" class="font-bold text-base">ชื่อ</th>
                            <td>{{ $model->name }}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="font-bold text-base">รายละเอียด</th>
                            <td>{{ $model->detail }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <form
            action="{{ $model->id ? route('raw-mat-type.update', ['raw_mat_type' => $model->id]) : route('raw-mat-type.store') }}"
            method="POST" class="card">
            @csrf
            @method($model->id ? 'patch': 'post')
            <div class="card-header">
                <label> ฟอร์มข้อมูล</label>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <tbody>
                        <tr class="flex">
                            <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ชื่อ</th>
                            <td class="w-full">
                                {!! Form::text('name', $model->name, ['class' => 'form-control', 'required' => true]) !!}
                            </td>
                        </tr>
                        <tr class="flex">
                            <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">รายละเอียด</th>
                            <td class="w-full">
                                {!! Form::textarea('detail', $model->detail, ['class' => 'form-control', 'required' => true]) !!}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            {!! Form::submit('บันทึกข้อมูล', ['class' => 'btn btn-success btn-lg text-white']) !!}
        </form>
    @endif
@endsection
