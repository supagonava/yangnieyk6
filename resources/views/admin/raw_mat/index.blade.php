@extends('layouts.admin.app')
@section('content')
    @php
    $columns = ['ชื่อ', 'หมวดหมู่', 'คงคลัง', 'จุดสั่งซื้อ'];
    $attributes = ['name', 'typeName', 'stock_quantity', 'point_of_order'];
    $options = ['data' => $models, 'card_title' => 'ตารางวัตถุดิบ', 'create' => 'raw-mat.create', 'show' => 'raw-mat.show', 'edit' => 'raw-mat.edit', 'delete' => 'raw-mat.destroy', 'param' => 'raw_mat', 'columns' => $columns, 'attributes' => $attributes];
    @endphp
    @include('admin.components.table', $options)
@endsection
