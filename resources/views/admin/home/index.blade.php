@extends('layouts.admin.app')

@section('content')

<div class="card">
    <div class="card-header">
        ฟอร์มค้นหา
    </div>
    <form class="card-body flex gap-2">
        <div class="form-group w-1/4">
            <label>วันที่เริ่มค้นหา</label>
            <input value="{{ $_GET['firstdate'] ?? null }}" class="form-control" type="date" name="firstdate" id="">
        </div>
        <div class="form-group w-1/4">
            <label>วันที่สิ้นสุดค้นหา</label>
            <input value="{{ $_GET['lastdate'] ?? null }}" class="form-control" type="date" name="lastdate" id="">
        </div>
        <div class="form-group w-1/4">
            <label>รูปแบบการแสดงผล</label>
            {!! Form::select('format', ['day'=>'วัน','month'=>'เดือน','year'=>'ปี'], $_GET['format'] ?? 'month',
            ['class' => 'form-select',]) !!}
        </div>
        <div class="h-20 mt-7 w-1/4 xl:w-1/6">
            <button class="btn btn-success w-full text-white">
                <i class="fa fa-search" aria-hidden="true"></i>
                ค้นหา
            </button>
        </div>
    </form>
</div>

<div class="row">
    <!-- Column -->
    <div class="col-md-6 col-lg-2 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-cyan text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-view-dashboard"></i></h1>
                <h6 class="text-white">จำนวนผู้ใช้งาน {{ $data['userCount'] }}</h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-4 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-success text-center">
                <h1 class="font-light text-white">
                    <i class="fas fa-dollar-sign    "></i>
                </h1>
                <h6 class="text-white">ยอดขาย {{ number_format($data['sum_order']) }} บาท</h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-2 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-warning text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                <h6 class="text-white">จำนวนสินค้า {{ $data['productCount'] }}</h6>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-2 col-xlg-3">
        <div class="card card-hover">
            <div class="box bg-danger text-center">
                <h1 class="font-light text-white"><i class="mdi mdi-border-outside"></i></h1>
                <h6 class="text-white">จำนวนโต๊ะ {{ $data['tableCount'] }}</h6>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div id="chart-container"></div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <h5 class="card-title mb-0">วัตถุดิบที่ถึงจุดสั่งซื้อ</h5>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">ชื่อ</th>
                    <th scope="col">คงเหลือ</th>
                    <th scope="col">จุดสั่งซื้อ</th>
                </tr>
            </thead>
            <tbody class="customtable">
                @forelse ($data['raw_mat_lower_stock'] as $rawmat)
                <tr>
                    <td>
                        <a href="{{ route('raw-mat.show',['raw_mat'=>$rawmat->id]) }}">{{ $rawmat->name }}</a>
                    </td>
                    <td>{{ $rawmat->stock_quantity }}</td>
                    <td>{{ $rawmat->point_of_order }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="3">
                        ไม่มีวัตถุดิบที่ถึงจุดสั่งซื้อ
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

<div class="grid grid-cols-5 gap-3">
    @foreach ($promotions as $model)
    <div class="xl:col-span-1 col-span-full w-full">
        <div class="card">
            <div class="card-body">
                <img class="w-full" src="{{ $model->photo() }}">
                <h4 class="mt-2">โปรโมชั่น {{ $model->title }}</h4>
                <form method="post" action="{{ route('promotion.destroy',['promotion'=>$model->id]) }}">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger text-white">ลบออก</button>
                </form>
            </div>
        </div>
    </div>
    @endforeach
    <div class="xl:col-span-1 col-span-full w-full">
        <div class="card">
            <div class="card-header">
                <h5>เพิ่มโปรโมชั่น</h5>
            </div>
            <form action="{{ route('promotion.store') }}" method="post" class="card-body" enctype="multipart/form-data">
                @csrf
                {!! Form::file('file_upload', ['class' => 'form-control','required'=>true]) !!}
                <div class="mt-2">ชื่อโปรฯ</div>
                {!! Form::text('title', '', ['class' => 'form-control','placeholder'=>'กรุณาระบุ',
                'required'=>true]) !!}
                <button class="btn btn-primary w-full mt-2" type="submit">บันทึก</button>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@javascript('series',$data['series'])
@javascript('categories',$data['categories'])

<script src="{{ mix('js/dashboard.js') }}"></script>
@endsection
