@extends('layouts.admin.app')

@section('content')
<div class="grid grid-cols-3 gap-1">
    <div class="col-span-full xl:col-span-2 bg-white rounded-lg p-2 border-gray-400 overflow-auto">
        <div class="relative">
            <div style="top:255px;left:282px" class="absolute h-44 w-44 bg-green-400">
                <h2 class="text-white w-full text-center pt-16">หน้าร้าน</h2>
            </div>
            <div style="top:0px;left:463px" class="absolute bg-green-400">
                <div style="width:17.5rem;height: 15.5rem;">
                    <h2 class="text-white w-full text-center pt-24">รายอาหาร</h2>
                </div>
            </div>
            <div class="absolute bg-green-400 transform rotate-90" style="top: 8.5rem; left: 38.5rem;">
                <div style="width: 19.5rem; height: 3rem;">
                    <h2 class="text-white w-full text-center pt-1.5">แคชเชียร์</h2>
                </div>
            </div>
            <div class="flex gap-2 flex-col" style="width: 800px">
                <div class="flex gap-2" v-for="row in 7">
                    <div class="w-14 h-14" @click="switchTable(row,col)" :class="{
                        'bg-yellow-600 hover:bg-yellow-400 cursor-pointer':getTable(row,col) != null && getTable(row,col).active && getTable(row,col).sale_order.status == 'จอง',
                        'bg-blue-400 hover:bg-blue-300 cursor-pointer':getTable(row,col) != null && getTable(row,col).active && getTable(row,col).sale_order.status == 'กำลังทาน',
                        'bg-gray-400 hover:bg-gray-300 cursor-pointer':getTable(row,col) != null && getTable(row,col).active && getTable(row,col).sale_order.status == null,
                        }" v-for="col in 14">
                        <p v-if="getTable(row,col)" class="text-white pt-3 pl-2 xl:pl-1">@{{ getTable(row,col).no }}</p>
                    </div>
                </div>
            </div>
            <div class="flex gap-1 mt-3">
                <div class="flex gap-1">
                    <div class="w-5 h-5 rounded-full bg-gray-400"></div>
                    โต๊ะว่าง
                </div>
                <div class="flex gap-1">
                    <div class="w-5 h-5 rounded-full bg-yellow-600"></div>
                    ติดจอง
                </div>
                <div class="flex gap-1">
                    <div class="w-5 h-5 rounded-full bg-blue-400"></div>
                    กำลังทาน
                </div>
            </div>
        </div>
    </div>

    <div v-if="activeTable" class="card col-span-full xl:col-span-1">
        <h3 class="card-header flex justify-between gap-2">
            <div>
                <i class="fa fa-table" aria-hidden="true"></i>
                โต๊ะที่ @{{ activeTable.no }}
            </div>
            <button type="button" @click="activeTable = null"><i class="fas fa-times"></i></button>
        </h3>
        <div class="card-body">
            <div class="flex flex-col p-2 gap-2">
                <h5>สถานะ : @{{ getTableStatus() ? activeTable.sale_order.status : 'โต๊ะว่าง'}}
                <div class="text-xs" v-if="getTableStatus() && activeTable.sale_order.status == 'จอง'">
                    เวลาจอง : @{{ activeTable.sale_order.appointment_at }}
                </div>
                </h5>
                <form action="{{ route('sale-order.submit') }}" onsubmit="return confirm('ยืนยันที่จะดำเนินการ?')"
                    method="POST">
                    @csrf
                    @method('put')
                    <div class="flex gap-2 justify-between mb-2">
                        <label>โดย @{{ activeTable.sale_order.user_name ? `${activeTable.sale_order.user_name} (${activeTable.sale_order.user_phone})` : 'ลูกค้าหน้าร้าน' }}</label>
                        <div class="flex gap-1">
                            <button type="button" class="btn btn-success text-white" @click="add()">
                                เพิ่มรายการ <i class="fa fa-plus-circle" aria-hidden="true">
                                </i>
                            </button>
                            <button type="button" @click="switchTable(activeTable.row,activeTable.col)"
                                class="btn btn-info text-white">
                                <i class="fa fa-recycle" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                    <input type="hidden" name="saleOrderId" v-model="activeTable.sale_order.id">
                    <input type="hidden" name="tableId" :value="activeTable.no">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>รายการ</th>
                                <th>จำนวน</th>
                                <th>ราคาสุทธิ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="active" v-for="(detail,index) in activeTable.sale_order.details" :key="index">
                                <td>
                                    <div class="flex gap-1 items-center">
                                        @{{ index+1 }}
                                        <input type="hidden" :name="`detail[${index}][id]`" v-model="detail.id">
                                        <select :name="`detail[${index}][product_id]`" class="form-select"
                                            v-model="detail.product_id">
                                            <option :value="product.id" v-for="product in products" :key="product.id">
                                                @{{ product.name }}
                                            </option>
                                        </select>
                                    </div>
                                </td>
                                <td class="w-28">
                                    <input type="number" :name="`detail[${index}][quantity]`"
                                        v-model.number="detail.quantity" class="form-control ">
                                </td>
                                <td>
                                    <div class="flex gap-2 items-center justify-between">
                                        <div>
                                            @{{toBath( detail.quantity * getProduct(detail.product_id).price)}}
                                        </div>
                                        <button class="text-red-500" type="button" @click="remove(detail)">
                                            <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right font-bold text-lg">
                                    <div class="flex justify-between gap-2">
                                        <div class="w-32 flex gap-1 justify-items-center">ส่วนลด
                                            <input type="number" v-model.number="activeTable.sale_order.discount"
                                                class="form-control " name="discount">
                                        </div>
                                        <label>รวม @{{ toBath(sumActiveTable) }}
                                            <input v-model="sumActiveTable" type="hidden" name="amount">
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="flex justify-between gap-2">
                        <button v-if="activeTable.sale_order.status == 'กำลังทาน'" type="submit" name="submit"
                            value="close" class="btn btn-warning w-full">คิดเงิน</button>
                        <button
                            v-if="activeTable.sale_order.status == 'จอง' || !getTableStatus() && activeTable.sale_order.details.length > 0"
                            type="submit" name="submit" value="open" class="btn btn-info w-full">เปิดโต๊ะ</button>
                        <button type="submit" name="submit" value="save" class="btn btn-primary w-full"
                            v-if="getTableStatus()">บันทึกข้อมูล</button>
                        <button type="submit" name="submit" value="cancle" class="btn btn-danger text-white w-full"
                            v-if="activeTable.sale_order.status != null">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
@javascript('tables',$tables)
@javascript('products',App\Models\Products::get())
@if (session('print'))
<script>
    window.open("{{ route('print-bill',['so'=>session('print')]) }}",'_blank');
</script>
@endif
<script src="{{ mix('js/pospos.js') }}"></script>
@endsection
