@extends('layouts.admin.app')
@section('content')
    @php
    $columns = [ 'ชื่อ', 'หมวดหมู่', 'ราคาขาย'];
    $attributes = ['name', 'categoryName', 'price'];
    $options = ['data' => $models, 'card_title' => 'ตารางสินค้า', 'create' => 'prod.create', 'show' => 'prod.show', 'edit' => 'prod.edit', 'delete' => 'prod.destroy', 'param' => 'prod', 'columns' => $columns, 'attributes' => $attributes];
    @endphp
    @include('admin.components.table', $options)
@endsection
