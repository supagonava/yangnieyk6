@extends('layouts.admin.app')

@section('content')
@if ($mode == 'show')
<div class="card">
    <div class="card-header flex justify-between items-center">
        <label class="text-base align-middle">การกระทำ</label>
        <div class="flex gap-1 justify-end">
            <a class="btn btn-info" href="{{ route('prod.edit', ['prod' => $model->id]) }}">
                <i class="fas fa-edit"></i>
            </a>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <tbody>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ชื่อ</th>
                    <td class="w-full">{{ $model->name }}</td>
                </tr>
                {{-- <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">
                        รูปภาพ
                    </th>
                    <td class="w-full">
                        <img src="{{ $model->photo() }}" class="h-56 w-56" />
                    </td>
                </tr> --}}
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">คงคลัง</th>
                    <td class="w-full">{{ $model->stock_quantity }}</td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">หน่วย</th>
                    <td class="w-full">{{ $model->unit }}</td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">จุดสั่งซื้อ</th>
                    <td class="w-full">{{ $model->point_of_order }}</td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ต้นทุน</th>
                    <td class="w-full">{{ $model->cost }}</td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ประเภท</th>
                    <td class="w-full">{{ $model->type_id }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@else
<form action="{{ $model->id ? route('prod.update', ['prod' => $model->id]) : route('prod.store') }}" method="POST"
    class="card" enctype="multipart/form-data">
    @csrf
    @method($model->id ? 'patch': 'post')
    <div class="card-header">
        <label> ฟอร์มข้อมูล</label>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <tbody>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">
                        ชื่อ
                        <br /><small class="text-xs text-red-500">*จำเป็น</small>
                    </th>
                    <td class="w-full">
                        {!! Form::text('name', $model->name, ['class' => 'form-control', 'required' => true]) !!}
                    </td>
                </tr>
                {{-- <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">
                        รูปภาพ
                    </th>
                    <td class="w-full">
                        <img src="{{ $model->photo() }}" class="h-56 w-56" />
                        {!! Form::file('upload_file', ['class' =>'form-control']) !!}
                    </td>
                </tr> --}}
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">
                        คงคลัง
                        <br /><small class="text-xs text-red-500"></small>
                    </th>
                    <td class="w-full">
                        {!! Form::number('stock_quantity', $model->stock_quantity, ['class' => 'form-control']) !!}
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">
                        หน่วย
                        <br /><small class="text-xs text-red-500">*จำเป็น</small>
                    </th>
                    <td class="w-full">
                        {!! Form::text('unit', $model->unit, ['class' => 'form-control', 'required' => true]) !!}
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">
                        จุดสั่งซื้อ
                        <br /><small class="text-xs text-red-500"></small>
                    </th>
                    <td class="w-full">
                        {!! Form::number('point_of_order', $model->point_of_order, ['class' => 'form-control']) !!}
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ต้นทุน</th>
                    <td class="w-full">
                        {!! Form::number('cost', $model->cost, ['class' => 'form-control']) !!}
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ราคาขาย</th>
                    <td class="w-full">
                        {!! Form::number('price', $model->price, ['class' => 'form-control']) !!}
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ประเภท</th>
                    <td class="w-full">
                        {!! Form::select('category_id', App\Models\ProductsCategories::get()->pluck('name', 'id'),
                        $model->type_id, ['class' => 'form-select shadow-none']) !!}
                </tr>
            </tbody>
        </table>
    </div>

    {!! Form::submit('บันทึกข้อมูล', ['class' => 'btn btn-success btn-lg text-white']) !!}
</form>
@endif

@endsection
