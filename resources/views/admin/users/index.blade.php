@extends('layouts.admin.app')
@section('content')
    @php
    $columns = ['ชื่อ', 'อีเมล์', 'บทบาท'];
    $attributes = ['name', 'email', 'role'];
    $options = ['data' => $models, 'card_title' => 'ตารางผู้ใช้', 'create' => 'user.create', 'show' => 'user.show', 'edit' => 'user.edit', 'delete' => 'user.destroy', 'param' => 'user', 'columns' => $columns, 'attributes' => $attributes];
    @endphp
    @include('admin.components.table', $options)
@endsection
