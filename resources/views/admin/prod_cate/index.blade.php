@extends('layouts.admin.app')
@section('content')
    @php
    $columns = ['ชื่อ'];
    $attributes = ['name'];
    $options = ['data' => $models, 'card_title' => 'ตารางหมวดหมู่สินค้า', 'create' => 'prod-cate.create', 'show' => 'prod-cate.show', 'edit' => 'prod-cate.edit', 'delete' => 'prod-cate.destroy', 'param' => 'prod_cate', 'columns' => $columns, 'attributes' => $attributes];
    @endphp
    @include('admin.components.table', $options)
@endsection
