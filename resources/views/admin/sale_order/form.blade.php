@extends('layouts.admin.app')

@section('content')
@if ($mode == 'show')
<div class="card">
    {{-- <div class="card-header flex justify-between items-center">
                <label class="text-base align-middle">การกระทำ</label>
                <div class="flex gap-1 justify-end">
                    <a class="btn btn-info" href="{{ route('sale-order.edit', ['sale_order' => $model->id]) }}">
    <i class="fas fa-edit"></i>
    </a>
</div>
</div> --}}
<div class="card-body">
    <table class="table table-hover">
        <tbody>
            <tr class="flex">
                <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">รหัส</th>
                <td class="w-full">{{ $model->id }}</td>
            </tr>
            <tr class="flex">
                <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">สถานะ</th>
                <td class="w-full">{{ $model->statusName }}</td>
            </tr>
            <tr class="flex">
                <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">โต๊ะ</th>
                <td class="w-full">{{ $model->tableNo }}</td>
            </tr>
            <tr class="flex">
                <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">รายละเอียด</th>
                <td class="w-full">
                    @forelse($model->details as $key => $value)
                    <div class="flex gap-2 justify-between">
                        <p>{{ $value->product->name }} ({{ $value->quantity }} {{ $value->product->unit }})</p>
                        <p>เป็นเงิน {{ $value->product->price * $value->quantity }} บาท</p>
                    </div>
                    <hr/>
                    @empty
                    ว่างเปล่า
                    @endforelse
                </td>
            </tr>
            <tr class="flex">
                <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ราคาสุทธิ</th>
                <td class="w-full">{{ $model->amount }}</td>
            </tr>
            <tr class="flex">
                <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ส่วนลด</th>
                <td class="w-full">{{ $model->discount }}</td>
            </tr>
            <tr class="flex">
                <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ลูกค้าผู้ทำรายการ</th>
                <td class="w-full">{{ $model->userName }}</td>
            </tr>
        </tbody>
    </table>
</div>
</div>
@else
<form action="{{ $model->id ? route('sale-order.update', ['sale_order' => $model->id]) : route('sale-order.store') }}"
    method="POST" class="card">
    @csrf
    @method($model->id ? 'patch': 'post')
    <div class="card-header">
        <label> ฟอร์มข้อมูล</label>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <tbody>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">รหัส</th>
                    <td class="w-full">{{ $model->id }}</td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">สถานะ</th>
                    <td class="w-full">
                        {!! Form::select('status', App\Models\SaleOrders::STATUS_LIST, $model->status, ['class' =>
                        'form-select shadow-none']) !!}
                    </td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">โต๊ะ</th>
                    <td class="w-full">
                        {!! Form::select('status', App\Models\Tables::get()->pluck('no', 'id'), $model->table_id,
                        ['class' => 'form-select shadow-none']) !!}
                    </td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ราคาสุทธิ</th>
                    <td class="w-full">
                        {!! Form::number('amount', $model->amount, ['class' => 'form-control']) !!}
                    </td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ส่วนลด</th>
                    <td class="w-full">
                        {!! Form::number('discount', $model->discount, ['class' => 'form-control']) !!}
                    </td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">ลูกค้าผู้ทำรายการ</th>
                    <td class="w-full">
                        {!! Form::select('user_id', App\Models\User::get()->pluck('name', 'id'), $model->user_id,
                        ['class' => 'form-select shadow-none']) !!}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    {!! Form::submit('บันทึกข้อมูล', ['class' => 'btn btn-success btn-lg text-white']) !!}
</form>
@endif
@endsection
