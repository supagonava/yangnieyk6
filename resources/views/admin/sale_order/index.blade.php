@extends('layouts.admin.app')
@section('content')
    @php
    $columns = ['รหัสคำสั่งซื้อ', 'ชื่อลูกค้า', 'รวม', 'ส่วนลด', 'สถานะ'];
    $attributes = ['id', 'userName', 'amount', 'discount', 'statusName'];
    $options = ['show_add'=>false, 'data' => $models,'notEdit'=>true, 'card_title' => 'ตารางการขาย', 'create' => 'sale-order.create', 'show' => 'sale-order.show', 'edit' => 'sale-order.edit', 'delete' => 'sale-order.destroy', 'param' => 'sale_order', 'columns' => $columns, 'attributes' => $attributes];
    @endphp
    @include('admin.components.table', $options)
@endsection
