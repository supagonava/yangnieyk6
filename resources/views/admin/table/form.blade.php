@extends('layouts.admin.app')

@section('content')
@if ($mode == 'show')
<div class="card">
    <div class="card-header flex justify-between items-center">
        <label class="text-base align-middle">การกระทำ</label>
        <div class="flex gap-1 justify-end">
            <a class="btn btn-info" href="{{ route('table.edit', ['table' => $model->id]) }}">
                <i class="fas fa-edit"></i>
            </a>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <tbody>
                <tr>
                    <th scope="row" class="font-bold text-base">เลขโต๊ะ</th>
                    <td>{{ $model->no }}</td>
                </tr>
                <tr>
                    <th scope="row" class="font-bold text-base">ตำแหน่ง</th>
                    <td>{{ $model->row }}/{{ $model->col }}</td>
                </tr>
                <tr>
                    <th scope="row" class="font-bold text-base">เปิดใช้งาน</th>
                    <td>{{ $model->active }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@else
<form action="{{ $model->id ? route('table.update', ['table' => $model->id]) : route('table.store') }}" method="POST"
    class="card">
    @csrf
    @method($model->id ? 'patch': 'post')
    <div class="card-header">
        <label> ฟอร์มข้อมูล</label>
    </div>
    <div class="card-body">
        <table class="table table-hover">
            <tbody>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">หมายเลขโต๊ะ</th>
                    <td class="w-full">
                        {!! Form::text('name', $model->no, ['class' => 'form-control', 'required' => true]) !!}
                    </td>
                </tr>
                <tr class="flex">
                    <th scope="row" class="font-bold text-base w-1/3 md:w-1/4">พิกัด</th>
                    <td class="w-full">
                        <div class="flex flex-col gap-1">
                            <div class="flex gap-1">
                                <p class="w-14 flex-grow-0">แถว</p>
                                {!! Form::number('row', $model->row, ['class' => 'form-control', 'required' => true])
                                !!}
                            </div>
                            <div class="flex gap-1">
                                <p class="w-14 flex-grow-0">คอลัมน์</p>
                                {!! Form::number('row', $model->row, ['class' => 'form-control', 'required' => true])
                                !!}
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    {!! Form::submit('บันทึกข้อมูล', ['class' => 'btn btn-success btn-lg text-white']) !!}
</form>
@endif
@endsection
