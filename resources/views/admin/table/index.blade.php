@extends('layouts.admin.app')
@section('content')

@php
$columns = ['หมายเลขโต๊ะ', 'เปิดใช้งาน'];
$attributes = ['no', 'activeText'];
$notShowAction = true;
$show_add=false;
$data = $models;
$card_title = 'ตารางหมวดหมู่โต๊ะ';
$create = 'table.create';
$show = 'table.show';
$edit = 'table.edit';
$delete = 'table.destroy';
$param = 'table';
@endphp

<div class="card">
    @if(!isset($show_add))
    <div class="flex items-center align-middle justify-between mb-2 card-header">
        <h5>{{ $card_title }}</h5>
        <a href="{{ route($create) }}" class="btn btn-success text-white">
            เพิ่ม <i class="fas fa-plus"></i>
        </a>
    </div>
    @endif
    <div class="card-body  overflow-x-auto max-w-full">
        <table class="table p-1">
            <thead class="thead-inverse">
                <tr class="text-center">
                    <th>เลขโต๊ะ</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($models as $model)
                <tr>
                    <td>{{ $model->no }}</td>
                    <td>
                        <div class="flex gap-2">
                            {{ $model->activeText }}
                            <form action="{{ route('table.toggle',['table'=>$model->id]) }}">
                                <button
                                    class="btn btn-{{ $model->active ? 'success': 'danger' }} text-white">คลิกเพื่อ{{ $model->active ? 'ปิด': 'เปิด' }}</button>
                            </form>
                        </div>
                    </td>
                    @if (empty($notShowAction))
                    <td>
                        <div class="flex gap-1 justify-end">
                            <a class="btn btn-info" href="{{ route($show, [$param = $model->id]) }}"><i
                                    class="fa fa-eye" aria-hidden="true"></i></a>
                            <a class="btn btn-warning" href="{{ route($edit, [$param = $model->id]) }}"><i
                                    class="fa fa-edit" aria-hidden="true"></i></a>
                            <form action="{{ route($delete, [$param = $model->id]) }}"
                                onsubmit="return confirm('คุณต้องการจะลบรายการดังกล่าวหรือไม่?')" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger text-white"><i class="fas fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                    @endif
                </tr>
                @empty
                <tr>
                    <td colspan="{{ sizeOf($columns ?? []) + 1 }}" class="text-center p-2">
                        รายการว่างเปล่า
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        {!! $models->appends($_GET)->links() !!}
    </div>
</div>

@endsection
