<!doctype html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    {{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}" /> --}}
    <style>
        .page-break {
            page-break-after: always;
        }

        @font-face {
            font-family: Sarabun;
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
            src: url("{{ asset('fonts/sarabun/THSarabun.ttf') }}") format("truetype");
        }

        @font-face {
            font-family: Sarabun;
            font-weight: bold;
            font-style: bold;
            font-variant: bold;
            src: url("{{ asset('fonts/sarabun/THSarabun Bold.ttf') }}") format("truetype");
        }

        body {
            font-family: 'Sarabun';
            font-size: 20px;
        }

    </style>
</head>

<body>
    <div style="text-align: center;font-size: 25px;font-weight: bold;">
        ร้านย่างเนย
        <br />@คลอง6
    </div>
    <hr />
    <b>รหัสรายการ {{ $data['id'] }}</b>
    <br />
    <b>เวลาเปิดโต๊ะ {{ $data['updated_at'] }}</b>
    <br />
    <b>เวลาพิมพ์ {{ date('Y-m-d H:i:s') }}</b>
    <br />
    <b>พนักงาน {{ Auth::user()->name }}</b>
    <br />
    <b>ลูกค้า: {{ $data['user_name'] }}</b>
    <hr />
    <table style="width: 100%">
        <thead>
            <tr>
                <th>
                    ลำดับ
                </th>
                <th>
                    รายการ
                </th>
                <th>
                    เป็นเงิน
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data['details'] as $detail)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td style="width:180px;">
                    {{ $detail['product_name'] }} {{ $detail['product_price'] . 'x' . $detail['quantity'] }}
                </td>
                <td style="width:80px;text-align: right;padding-right: 10px">
                    {{ $detail['product_price'] * $detail['quantity'] }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <hr />
    <div style="text-align: right; width:100%;">
        ส่วนลด {{ $data['discount'] }} รวม {{ $data['amount'] }}
    </div>
    <hr />
    <div style="text-align: center">ขอบคุณที่มาอุดหนุนนะคะ</div>

</body>

</html>
