@extends('layouts.public.app')

@section('content')


@if(!empty($promotions))
<div class="mt-3">
    <div id="carouselId" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach ($promotions as $model)
            <li data-target="#carouselId" data-slide-to="{{ $loop->iteration-1 }}"
                class="{{ $loop->iteration == 1 ? 'active' : '' }}"></li>
            @endforeach
        </ol>
        <div class="carousel-inner" role="listbox">
            @foreach ($promotions as $model)
            <div class="carousel-item {{ $loop->iteration == 1 ? 'active' : '' }}">
                <img class="h-80 w-full object-center object-contain bg-gray-50" src="{{ $model->photo() }}"
                    alt="{{ $model->title }}">
                <div class="carousel-caption opacity-60 d-none d-md-block bg-gray-50">
                    <h3 class="text-black">{{ $model->title }}</h3>
                </div>
            </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
@endif


<div class="flex flex-col md:grid md:grid-cols-3 pt-3 px-2 h-full">

    <div class="col-span-full md:col-span-2 text-center bg-white p-2 overflow-auto">
        <h5>คลิกที่โต๊ะเพื่อทำการจอง</h5>
        <div class="relative">
            <div style="top:255px;left:282px" class="absolute h-44 w-44 bg-green-400">
                <h2 class="text-white w-full text-center pt-16">หน้าร้าน</h2>
            </div>
            <div style="top:0px;left:463px" class="absolute bg-green-400">
                <div style="width:17.5rem;height: 15.5rem;">
                    <h2 class="text-white w-full text-center pt-24">รายอาหาร</h2>
                </div>
            </div>
            <div class="absolute bg-green-400 transform rotate-90" style="top: 8.5rem; left: 38.5rem;">
                <div style="width: 19.5rem; height: 3rem;">
                    <h2 class="text-white w-full text-center pt-1.5">แคชเชียร์</h2>
                </div>
            </div>
            <div class="flex gap-2 flex-col" style="width: 800px">
                <div class="flex gap-2" v-for="row in 7">
                    <div class="w-14 h-14" @click="switchTable(row,col)" :class="{
                                'bg-yellow-600 hover:bg-yellow-400 cursor-pointer':getTable(row,col) != null && getTable(row,col).active && getTable(row,col).sale_order.status,
                                'bg-gray-400 hover:bg-gray-300 cursor-pointer':getTable(row,col) != null && getTable(row,col).active && !getTable(row,col).sale_order.status,
                                }" v-for="col in 14">
                        <p v-if="getTable(row,col)" class="text-white pt-3 pl-2 md:pl-1">
                            @{{ getTable(row,col).no }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="flex gap-1 mt-3">
                <div class="flex gap-1">
                    <div class="w-5 h-5 rounded-full bg-gray-400"></div>
                    โต๊ะว่าง
                </div>
                <div class="flex gap-1">
                    <div class="w-5 h-5 rounded-full bg-yellow-600"></div>
                    ติดจอง
                </div>
            </div>
        </div>
    </div>
    <div v-if="activeTable" class="card col-span-full md:col-span-1 mt-2">
        <h5 class="card-header flex justify-between gap-2">
            <div>
                <i class="fa fa-table" aria-hidden="true"></i>
                โต๊ะที่ @{{ activeTable.no }}
            </div>
            <button type="button" @click="activeTable = null"><i class="fas fa-times"></i></button>
        </h5>
        <div class="card-body">
            <div class="flex flex-col p-2 gap-2">
                <h5>สถานะ : @{{ getTableStatus() ? activeTable.sale_order.status : 'โต๊ะว่าง'}}</h5>
                <form class="w-full" action="{{ route('sale-order.submit') }}"
                    onsubmit="return confirm('หากลูกค้ามาช้ากว่าเวลาที่จอง 30 นาที ทางร้านขออนุญาติปล่อยโต๊ะให้ลูกค้าหน้าร้าน ขอบคุณค่ะ')" method="POST">
                    @csrf
                    @method('put')
                    <div class="flex gap-2 justify-between mb-2">
                        <div class="flex gap-1">
                            <button type="button" class="btn btn-success text-white" @click="add()">
                                เพิ่มรายการเพื่อจอง <i class="fa fa-plus-circle" aria-hidden="true">
                                </i>
                            </button>
                            <button type="button" @click="switchTable(activeTable.row,activeTable.col)"
                                class="btn btn-info text-white">
                                <i class="fa fa-recycle" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    <div class="flex flex-col gap-2">
                        <label class="w-24">เวลาที่จอง : </label>
                        <input type="datetime-local" required name="appointment_at" class="form-control w-60">
                    </div>
                    <input type="hidden" name="saleOrderId" v-model="activeTable.sale_order.id">
                    <input type="hidden" name="tableId" :value="activeTable.no">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>รายการ</th>
                                <th>จำนวน</th>
                                <th>ราคาสุทธิ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="active" v-for="(detail,index) in activeTable.sale_order.details" :key="index">
                                <td>
                                    <div class="flex gap-1 items-center">
                                        <input type="hidden" :name="`detail[${index}][id]`" v-model="detail.id">
                                        <select :name="`detail[${index}][product_id]`" class="form-select"
                                            v-model="detail.product_id">
                                            <option :value="product.id" v-for="product in products" :key="product.id">
                                                @{{ product.name }}
                                            </option>
                                        </select>
                                    </div>
                                </td>
                                <td class="w-10 md:w-24">
                                    <input type="number" :name="`detail[${index}][quantity]`"
                                        v-model.number="detail.quantity" class="form-control ">
                                </td>
                                <td>
                                    <div class="flex gap-2 items-center justify-between">
                                        <div>
                                            @{{toBath( detail.quantity * getProduct(detail.product_id).price)}}
                                        </div>
                                        <button class="text-red-500" type="button" @click="remove(detail)">
                                            <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right font-bold text-lg">
                                    <div class="flex justify-between gap-2">
                                        <input type="hidden" v-model.number="activeTable.sale_order.discount"
                                            class="form-control " name="discount">
                                        <label>รวม @{{ toBath(sumActiveTable) }}
                                            <input v-model="sumActiveTable" type="hidden" name="amount">
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="flex justify-between gap-2">
                        <button type="submit" name="submit" value="reservation"
                            :disabled="activeTable.sale_order.details.length <= 0"
                            class="btn btn-primary w-full">จอง</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@if (Auth::user())
<div class="card mt-2 mb-2">
    <div class="card-header">
        <h5>ประวัติการทำรายการ</h5>
    </div>
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>
                    <div class="font-bold">โต๊ะที่จอง</div>
                </th>
                <th>
                    <div class="font-bold">เวลาที่จอง</div>
                </th>
                <th>
                    <div class="font-bold">เป็นเงิน</div>
                </th>
            </tr>
        </thead>
        <tbody>
            @forelse ($orders as $order)
            <tr>
                <td>{{ $order['table_no'] }}</td>
                <td>{{ $order['appointment_at'] }}</td>
                <td>
                    {{ $order['amount'] }}
                    <p>{{ $order['status'] }}</p>
                    @if($order['status'] == 'จอง')
                    <form action="{{ route('sale-order.cancle',['so'=>$order['id']])}}"
                        onsubmit="return confirm('ยืนยันที่จะยกเลิกหรือไม่')" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-md btn-danger text-white rounded-xl">
                            กดเพื่อยกเลิก</button>
                    </form>
                    @endif
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="5">ยังไม่มีรายการใดๆ </td>
            </tr>
            @endforelse
        </tbody>
    </table>
    {{ $paginate->links() }}
</div>
@endif
@endsection

@section('scripts')
@javascript('tables',$tables)
@javascript('products',App\Models\Products::get())
@javascript('auth',Auth::user()->id ?? null)
@if (session('print'))
<script>
    window.open("{{ route('print-bill',['so'=>session('print')]) }}",'_blank');
</script>
@endif
<script src="{{ mix('js/pub_pospos.js') }}"></script>
@endsection
