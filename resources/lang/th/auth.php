<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'บัญชีผู้ใช้งานไม่ถูกต้อง',
    'password' => 'รหัสผ่านไม่ถูกต้อง',
    'throttle' => 'โปรดลองเข้าสู่ระบบอีกครั้งใน :seconds วินาที.',

];
