<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'ตั้งรหัสผ่านใหม่เรียบร้อยแล้ว',
    'sent' => 'เราส่งลิงค์สำหรับตั้งรหัสผ่านให้ท่านผ่านอีเมล์เรียบร้อยแล้ว',
    'throttled' => 'โปรดรอสักครู่และลองอีกครั้งครับ',
    'token' => 'โทเค่นไม่ถูกต้อง',
    'user' => "อีเมล์ดังกล่าวยังไม่ถูกลงทะเบียน",

];
