<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RawMaterials extends Model
{
    use HasFactory;
    protected $fillable = [
        "name",
        "stock_quantity",
        "unit",
        "point_of_order",
        "cost",
        "type_id",
        "updated_via",
    ];

    public function getTypeNameAttribute()
    {
        return $this->type->name;
    }

    public function type()
    {
        return $this->belongsTo(RawMaterialTypes::class, 'type_id');
    }
}
