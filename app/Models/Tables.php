<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tables extends Model
{
    use HasFactory;
    protected $fillable = [
        "no",
        "row",
        "col",
        "active"
    ];

    public function saleOrders()
    {
        return $this->hasMany(SaleOrders::class, 'table_id', 'id');
    }

    public function currentOrder()
    {
        return $this->saleOrders()->whereIn('status', [0, 1])->first();
    }

    public function getActiveTextAttribute()
    {
        return $this->active ? 'เปิดใช้งาน' : 'ปิดใช้งาน';
    }
}
