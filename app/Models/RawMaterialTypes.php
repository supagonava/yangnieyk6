<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RawMaterialTypes extends Model
{
    use HasFactory;

    protected $primaryKey = "id";
    protected $fillable = [
        "id",
        "name",
        "detail",
    ];
}
