<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Products extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        "name",
        "stock_quantity",
        "unit",
        "point_of_order",
        "cost",
        'price',
        "category_id",
        "updated_via",
    ];

    public function category()
    {
        return $this->belongsTo(ProductsCategories::class, 'category_id', 'id');
    }

    public function getCategoryNameAttribute()
    {
        return !empty($this->category) ? $this->category->name : 'ไม่มีหมวดหมู่';
    }

    public function photo()
    {
        $media = $this->getMedia('photo')->first();
        if ($media) {
            return $media->getUrl();
        }
        return asset('images/logo.jpg');
    }

    public function getPhotoAttribute()
    {
        return $this->photo();
    }
}
