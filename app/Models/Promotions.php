<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Promotions extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        "title",
        "detail",
    ];

    public function photo()
    {
        $media = $this->getMedia('photo')->first();
        return $media ? $media->getUrl() : asset('images/logo.jpg');
    }
}
