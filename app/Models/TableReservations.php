<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TableReservations extends Model
{
    use HasFactory;
    protected $fillable = [
        "user_id",
        "table_id",
        "reservation_at",
        "comment",
    ];
}
