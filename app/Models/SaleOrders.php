<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleOrders extends Model
{
    const STATUS_LIST = [0 => "จอง", 1 => "กำลังทาน", 2 => "ชำระเงินแล้ว", 3 => "ยกเลิก"];

    use HasFactory;

    protected $fillable = [
        'table_id',
        'amount',
        'discount',
        'appointment_at',
        'user_id',
        'status'
    ];

    protected $casts = [
        'amount' => 'int',
    ];

    public function details()
    {
        return $this->hasMany(SaleOrderDetail::class, 'sale_order_id');
    }

    public function orderTable()
    {
        return $this->belongsTo(Tables::class, 'table_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getUserNameAttribute()
    {
        return $this->user ? $this->user->name : 'ลูกค้าหน้าร้าน';
    }

    public function getTableNoAttribute()
    {
        return $this->orderTable->no;
    }

    public function getStatusNameAttribute()
    {
        return self::STATUS_LIST[$this->status];
    }
}
