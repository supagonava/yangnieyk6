<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class SaleOrderDetailsTrans extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($data)
    {
        $data->with('product');
        return [
            'id' => $data->id,
            'quantity' => $data->quantity,
            'amount' => $data->product->price * $data->quantity,
            'product_id' => $data->product->id,
            'product_name' => $data->product->name,
            'product_price' => $data->product->price,
            'photo' => $data->product->photo()
        ];
    }
}
