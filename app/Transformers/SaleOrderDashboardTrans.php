<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class SaleOrderDashboardTrans extends TransformerAbstract
{
    public $type = 'month';
    public function __construct($type)
    {
        $this->type = $type;
    }
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($data)
    {
        $format = "Y M";
        if ($this->type == "day") {
            $format = "Y M d";
        }
        if ($this->type == "year") {
            $format = "Y";
        }
        return [
            'date' => Carbon::createFromFormat("Y-m-d H:i:s", $data->created_at)->format($format),
            'amount' => $data->amount
        ];
    }
}
