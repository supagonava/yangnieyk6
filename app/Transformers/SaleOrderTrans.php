<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class SaleOrderTrans extends TransformerAbstract
{
    public $withTableNo = false;

    public function __construct($withTableNo = false)
    {
        $this->withTableNo = $withTableNo;
    }
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($data)
    {
        return [
            'id' => $data->id,
            'user_id' => $data->user_id,
            'user_name' => $data->userName,
            'user_phone' => !empty($data->user) ? $data->user->phone : '-',
            'amount' => $data->amount,
            'status' => $data->statusName,
            'discount' => $data->discount ?? 0,
            'details' => fractal($data->details, new SaleOrderDetailsTrans())->toArray()['data'],
            'appointment_at' => $data->appointment_at,
            'table_no' => $this->withTableNo ? $data->orderTable->no : null,
            'updated_at' => $data->updated_at
        ];
    }
}
