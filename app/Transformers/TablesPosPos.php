<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class TablesPosPos extends TransformerAbstract
{
    public $showDetail = true;
    public function __construct($showDetail = true)
    {
        $this->showDetail = $showDetail;
    }
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($data)
    {
        $fractData = [
            'id' => $data->id,
            'no' => $data->no,
            'row' => $data->row,
            'col' => $data->col,
            'active' => $data->active,
        ];

        if ($this->showDetail) {
            $fractData['sale_order'] = $data->currentOrder() ? fractal($data->currentOrder(), new SaleOrderTrans())->toArray()['data'] : [];
        } else {
            $fractData['sale_order'] = [
                'status' => !empty($data->currentOrder())
            ];
        }
        return $fractData;
    }
}
