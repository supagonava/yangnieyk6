<?php

namespace App;

use Illuminate\Support\Facades\Auth;

class SiteInfo
{
    public static function menus()
    {
        if (Auth::user()->role_id == 30) {
            return [
                ['label' => "หน้าหลัก", 'icon' => '<i class="fas fa-tachometer-alt"></i>', 'route' => route("home.index")],
                ['label' => "หน้าร้าน", 'icon' => '<i class="fa fa-calculator" aria-hidden="true"></i>', 'route' => route("home.pospos")],
                ['label' => "ผู้ใช้", 'icon' => '<i class="fas fa-user"></i>', 'route' => route('user.index')],
                [
                    'label' => 'ระบบสินค้า', 'icon' => '<i class="fa fa-shopping-bag"></i>',
                    'items' => [
                        ['label' => "ประเภทวัตถุดิบ", 'icon' => '<i class="fas fa-object-group"></i>', 'route' => route('raw-mat-type.index')],
                        ['label' => "ประเภทสินค้า", 'icon' => '<i class="fas fa-object-group"></i>', 'route' => route('prod-cate.index')],
                        ['label' => "วัตถุดิบ", 'icon' => '<i class="fa fa-shopping-bag"></i>', 'route' => route('raw-mat.index')],
                        ['label' => "สินค้า", 'icon' => '<i class="fa fa-shopping-bag"></i>', 'route' => route('prod.index')],
                    ]
                ],
                [
                    'label' => 'ระบบจัดการร้าน', 'icon' => '<i class="fas fa-dollar-sign    "></i>',
                    'items' => [
                        ['label' => "โต๊ะ", 'icon' => '<i class="fas fa-table"></i>', 'route' => route('table.index')],
                        ['label' => "รายงานการขาย", 'icon' => '<i class="fa fa-list" aria-hidden="true"></i>', 'route' => route('sale-order.index')],
                    ]
                ],
            ];
        }
    }
}
