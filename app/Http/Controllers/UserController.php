<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $title = "จัดการผู้ใช้งาน";
        $models = User::query();
        if ($request->search) {
            $models->where('name', 'like', "%{$request->search}%")->orWhere('email', 'like', "%{$request->search}%");
        }
        $models = $models->latest()->paginate($request->perPage ?? 30);
        return view('admin.users.index', compact('models', 'title'));
    }

    public function create()
    {
        $model = new User();
        return view('admin.users.form', ['mode' => 'create', 'model' => $model, 'title' => 'เพิ่มผู้ใช้งานระบบ']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }
        $model = User::create($data);
        return redirect()->route('user.show', ['user' => $model])->with('success', 'เพิ่มผู้ใช้งานสำเร็จ');
    }

    public function show(User $user)
    {
        return view('admin.users.form', ['mode' => 'show', 'model' => $user, 'title' => 'ดูข้อมูล ' . $user->name]);
    }

    public function edit(User $user)
    {
        return view('admin.users.form', ['mode' => 'edit', 'model' => $user, 'title' => 'แก้ไขข้อมูล ' . $user->name]);
    }

    public function update(Request $request, User $user)
    {
        $data = $request->all();
        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        $user->update($data);
        return redirect()->route('user.index')->with('success', 'แก้ไขผู้ใช้งานสำเร็จ');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('user.index')->with('success', 'ลบผู้ใช้งานสำเร็จ');
    }
}
