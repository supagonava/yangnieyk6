<?php

namespace App\Http\Controllers;

use App\Models\Tables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TablesController extends Controller
{
    public function index(Request $request)
    {
        $title = "จัดการโต๊ะ";
        $models = Tables::query();
        if ($request->search) {
            $models->where('no', 'like', "%{$request->search}%");
        }
        $models = $models->latest()->paginate($request->perPage ?? 30);
        return view('admin.table.index', compact('models', 'title'));
    }

    public function create()
    {
        $model = new Tables();
        return view('admin.table.form', ['mode' => 'create', 'model' => $model, 'title' => 'เพิ่มโต๊ะ']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $model = Tables::create($data);
        return redirect()->route('table.show', ['table' => $model])->with('success', 'เพิ่มโต๊ะสำเร็จ');
    }

    public function show(Tables $table)
    {
        return view('admin.table.form', ['mode' => 'show', 'model' => $table, 'title' => 'ดูข้อมูล ' . $table->name]);
    }

    public function edit(Tables $table)
    {
        return view('admin.table.form', ['mode' => 'edit', 'model' => $table, 'title' => 'แก้ไขข้อมูล ' . $table->name]);
    }

    public function update(Request $request, Tables $table)
    {
        $data = $request->all();
        $table->update($data);
        return redirect()->route('table.index')->with('success', 'แก้ไขโต๊ะสำเร็จ');
    }

    public function destroy(Tables $table)
    {
        $table->delete();
        return redirect()->route('table.index')->with('success', 'ลบโต๊ะสำเร็จ');
    }

    public function toggleTable(Tables $table)
    {
        $table->active = !$table->active;
        $table->save();
        return redirect()->route('table.index')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }
}
