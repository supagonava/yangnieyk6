<?php

namespace App\Http\Controllers;

use App\Models\RawMaterialTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RawMaterialTypesController extends Controller
{
    public function index(Request $request)
    {
        $title = "จัดการหมวดหมู่วัตถุดิบ";
        $models = RawMaterialTypes::query();
        if ($request->search) {
            $models->where('name', 'like', "%{$request->search}%");
        }
        $models = $models->latest()->paginate($request->perPage ?? 30);
        return view('admin.raw_mat_types.index', compact('models', 'title'));
    }

    public function create()
    {
        $model = new RawMaterialTypes();
        return view('admin.raw_mat_types.form', ['mode' => 'create', 'model' => $model, 'title' => 'เพิ่มหมวดหมู่วัตถุดิบระบบ']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $model = RawMaterialTypes::create($data);
        return redirect()->route('raw-mat-type.show', ['raw_mat_type' => $model])->with('success', 'เพิ่มหมวดหมู่วัตถุดิบสำเร็จ');
    }

    public function show(RawMaterialTypes $raw_mat_type)
    {
        return view('admin.raw_mat_types.form', ['mode' => 'show', 'model' => $raw_mat_type, 'title' => 'ดูข้อมูล ' . $raw_mat_type->name]);
    }

    public function edit(RawMaterialTypes $raw_mat_type)
    {
        return view('admin.raw_mat_types.form', ['mode' => 'edit', 'model' => $raw_mat_type, 'title' => 'แก้ไขข้อมูล ' . $raw_mat_type->name]);
    }

    public function update(Request $request, RawMaterialTypes $raw_mat_type)
    {
        $data = $request->all();
        $raw_mat_type->update($data);
        return redirect()->route('raw-mat-type.index')->with('success', 'แก้ไขหมวดหมู่วัตถุดิบสำเร็จ');
    }

    public function destroy(RawMaterialTypes $raw_mat_type)
    {
        $raw_mat_type->delete();
        return redirect()->route('raw-mat-type.index')->with('success', 'ลบหมวดหมู่วัตถุดิบสำเร็จ');
    }
}
