<?php

namespace App\Http\Controllers;

use App\Models\RawMaterials;
use Illuminate\Http\Request;

class RawMaterialsController extends Controller
{
    public function index(Request $request)
    {
        $title = "จัดการวัตถุดิบ";
        $models = RawMaterials::query();
        if ($request->search) {
            $models->where('name', 'like', "%{$request->search}%");
        }
        $models = $models->latest()->paginate($request->perPage ?? 30);
        return view('admin.raw_mat.index', compact('models', 'title'));
    }

    public function create()
    {
        $model = new RawMaterials();
        return view('admin.raw_mat.form', ['mode' => 'create', 'model' => $model, 'title' => 'เพิ่มวัตถุดิบ']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $model = RawMaterials::create($data);
        return redirect()->route('raw-mat.show', ['raw_mat' => $model])->with('success', 'เพิ่มวัตถุดิบสำเร็จ');
    }

    public function show(RawMaterials $raw_mat)
    {
        return view('admin.raw_mat.form', ['mode' => 'show', 'model' => $raw_mat, 'title' => 'ดูข้อมูล ' . $raw_mat->name]);
    }

    public function edit(RawMaterials $raw_mat)
    {
        return view('admin.raw_mat.form', ['mode' => 'edit', 'model' => $raw_mat, 'title' => 'แก้ไขข้อมูล ' . $raw_mat->name]);
    }

    public function update(Request $request, RawMaterials $raw_mat)
    {
        $data = $request->all();
        $raw_mat->update($data);
        return redirect()->route('raw-mat.index')->with('success', 'แก้ไขวัตถุดิบสำเร็จ');
    }

    public function destroy(RawMaterials $raw_mat)
    {
        $raw_mat->delete();
        return redirect()->route('raw-mat.index')->with('success', 'ลบวัตถุดิบสำเร็จ');
    }
}
