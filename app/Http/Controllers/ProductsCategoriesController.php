<?php

namespace App\Http\Controllers;

use App\Models\ProductsCategories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProductsCategoriesController extends Controller
{
    public function index(Request $request)
    {
        $title = "จัดการหมวดหมวดหมู่สินค้า";
        $models = ProductsCategories::query();
        if ($request->search) {
            $models->where('name', 'like', "%{$request->search}%");
        }
        $models = $models->latest()->paginate($request->perPage ?? 30);
        return view('admin.prod_cate.index', compact('models', 'title'));
    }

    public function create()
    {
        $model = new ProductsCategories();
        return view('admin.prod_cate.form', ['mode' => 'create', 'model' => $model, 'title' => 'เพิ่มหมวดหมวดหมู่สินค้า']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $model = ProductsCategories::create($data);
        return redirect()->route('prod-cate.show', ['prod_cate' => $model])->with('success', 'เพิ่มหมวดหมวดหมู่สินค้าสำเร็จ');
    }

    public function show(ProductsCategories $prod_cate)
    {
        return view('admin.prod_cate.form', ['mode' => 'show', 'model' => $prod_cate, 'title' => 'ดูข้อมูล ' . $prod_cate->name]);
    }

    public function edit(ProductsCategories $prod_cate)
    {
        return view('admin.prod_cate.form', ['mode' => 'edit', 'model' => $prod_cate, 'title' => 'แก้ไขข้อมูล ' . $prod_cate->name]);
    }

    public function update(Request $request, ProductsCategories $prod_cate)
    {
        $data = $request->all();
        $prod_cate->update($data);
        return redirect()->route('prod-cate.index')->with('success', 'แก้ไขหมวดหมวดหมู่สินค้าสำเร็จ');
    }

    public function destroy(ProductsCategories $prod_cate)
    {
        $prod_cate->delete();
        return redirect()->route('prod-cate.index')->with('success', 'ลบหมวดหมวดหมู่สินค้าสำเร็จ');
    }
}
