<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Promotions;
use App\Models\RawMaterials;
use App\Models\SaleOrders;
use App\Models\Tables;
use App\Models\User;
use App\Transformers\SaleOrderDashboardTrans;
use App\Transformers\SaleOrderTrans;
use App\Transformers\TablesPosPos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index()
    {
        $tables = Tables::get();
        $tables = fractal($tables, new TablesPosPos($showDetail = false))->toArray()['data'];
        if (Auth::user()) {
            $paginate = SaleOrders::where('user_id', Auth::user()->id)->latest()->paginate(5);
            $orders = fractal($paginate, new SaleOrderTrans($withTableNo = true))->toArray()['data'];
        }
        $promotions = Promotions::get();
        return view('public.home', ['tables' => $tables, 'orders' => $orders ?? [], 'paginate' => $paginate ?? [], 'promotions' => $promotions]);
    }

    public function adminIndex(Request $request)
    {
        if (Auth::user() && Auth::user()->role_id == 30) {
            $promotions = Promotions::get();
            $dashboardData = [];

            $dashboardData['userCount'] = User::count();
            $dashboardData['productCount'] = Products::count();
            $dashboardData['tableCount'] = Tables::count();
            $orders = SaleOrders::orderBy('created_at');
            if ($request->firstdate) {
                $orders->whereDate('created_at', '>=', "$request->firstdate");
            }

            if ($request->lastdate) {
                $orders->whereDate('created_at', '<=', "$request->lastdate" . " 23:59:00");
            }

            $dashboardData['sum_order'] = $orders->sum('amount');

            if ($request->format == 'day') {
                $orders->groupByRaw('day(created_at), month(created_at), year(created_at)');
            } else if ($request->format == 'year') {
                $orders->groupByRaw('year(created_at)');
            } else {
                $orders->groupByRaw('month(created_at), year(created_at)');
            }

            $orders->select("created_at", DB::raw('sum(amount) as amount'));
            $orders = fractal($orders->get(), new SaleOrderDashboardTrans($request->format ?? 'month'))->toArray()['data'];

            $categories = collect($orders)->pluck('date');
            $series = [['name' => 'ยอดขาย', 'data' => collect($orders)->pluck('amount')]];

            $dashboardData['categories'] = $categories;
            $dashboardData['series'] = $series;

            $dashboardData['raw_mat_lower_stock'] = RawMaterials::whereRaw('stock_quantity <= point_of_order')->orderBy('name')->paginate(10);
            return view('admin.home.index', ['title' => 'หน้าหลัก', 'promotions' => $promotions, 'data' => $dashboardData]);
        }
        echo "คุณไม่ได้รับอนุญาตให้เข้าถึงหน้าดังกล่าว";
    }

    public function pospos()
    {
        $tables = Tables::get();
        $tables = fractal($tables, new TablesPosPos($showDetail = true))->toArray()['data'];
        return view('admin.home.pospos', ['title' => 'ระบบขายหน้าร้าน', 'tables' => $tables]);
    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('public.index');
    }

    public function howToUse()
    {
        return view('public.how_to_use');
    }
}
