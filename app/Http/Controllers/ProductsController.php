<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $title = "จัดการสินค้า";
        $models = Products::query();
        if ($request->search) {
            $models->where('name', 'like', "%{$request->search}%");
        }
        $models = $models->latest()->paginate($request->perPage ?? 30);
        return view('admin.prod.index', compact('models', 'title'));
    }

    public function create()
    {
        $model = new Products();
        return view('admin.prod.form', ['mode' => 'create', 'model' => $model, 'title' => 'เพิ่มสินค้า']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $model = Products::create($data);
        if ($request->hasFile('upload_file')) {
            $model->addMedia($request->upload_file)->toMediaCollection('photo');
        }
        return redirect()->route('prod.show', ['prod' => $model])->with('success', 'เพิ่มสินค้าสำเร็จ');
    }

    public function show(Products $prod)
    {
        return view('admin.prod.form', ['mode' => 'show', 'model' => $prod, 'title' => 'ดูข้อมูล ' . $prod->name]);
    }

    public function edit(Products $prod)
    {
        return view('admin.prod.form', ['mode' => 'edit', 'model' => $prod, 'title' => 'แก้ไขข้อมูล ' . $prod->name]);
    }

    public function update(Request $request, Products $prod)
    {
        $data = $request->all();
        $prod->update($data);
        if ($request->hasFile('upload_file')) {
            $media = $prod->getMedia('photo')->first();
            if ($media) {
                $media->delete();
            }
            $prod->addMedia($request->upload_file)->toMediaCollection('photo');
        }
        return redirect()->route('prod.index')->with('success', 'แก้ไขสินค้าสำเร็จ');
    }

    public function destroy(Products $prod)
    {
        $prod->delete();
        return redirect()->route('prod.index')->with('success', 'ลบสินค้าสำเร็จ');
    }
}
