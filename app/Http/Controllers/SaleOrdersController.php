<?php

namespace App\Http\Controllers;

use App\Models\SaleOrderDetail;
use App\Models\SaleOrders;
use App\Transformers\SaleOrderTrans;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SaleOrdersController extends Controller
{
    public function index(Request $request)
    {
        $title = "จัดการสินค้า";
        $models = SaleOrders::query();
        if ($request->search) {
            $models->where('id', 'like', "%{$request->search}%");
        }

        $models = $models->latest()->paginate($request->perPage ?? 30);
        return view('admin.sale_order.index', compact('models', 'title'));
    }

    public function create()
    {
        $model = new SaleOrders();
        return view('admin.sale_order.form', ['mode' => 'create', 'model' => $model, 'title' => 'เพิ่มสินค้า']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $model = SaleOrders::create($data);
        return redirect()->route('sale-order.show', ['sale_order' => $model])->with('success', 'เพิ่มสินค้าสำเร็จ');
    }

    public function show(SaleOrders $sale_order)
    {
        return view('admin.sale_order.form', ['mode' => 'show', 'model' => $sale_order, 'title' => 'ดูข้อมูล ' . $sale_order->name]);
    }

    public function edit(SaleOrders $sale_order)
    {
        return view('admin.sale_order.form', ['mode' => 'edit', 'model' => $sale_order, 'title' => 'แก้ไขข้อมูล ' . $sale_order->name]);
    }

    public function update(Request $request, SaleOrders $sale_order)
    {
        $data = $request->all();
        $sale_order->update($data);
        return redirect()->route('sale-order.index')->with('success', 'แก้ไขสินค้าสำเร็จ');
    }

    public function destroy(SaleOrders $sale_order)
    {
        $sale_order->delete();
        return redirect()->route('sale-order.index')->with('success', 'ลบสินค้าสำเร็จ');
    }

    public function submitOrder(Request $request)
    {
        $so = new SaleOrders();
        if ($request->saleOrderId) {
            $so = SaleOrders::where('id', $request->saleOrderId)->firstOrfail();
        } else {
            $so->table_id = $request->tableId;
        }
        switch ($request->submit) {
            case 'close':
                $so->status = 2;
                break;
            case 'open':
                $so->status = 1;
                break;
            case 'cancle':
                $so->status = 3;
            case 'save':
                $so->status = $so->status;
                break;
            default:
                $so->status = 0;
                break;
        }

        $so->amount = $request->amount;

        if (Auth::user()->role_id == 20) {
            $so->status = 0;
            $so->appointment_at = $request->appointment_at;
            $so->user_id = Auth::user()->id;
        }

        $so->save();
        // dd($request->all());
        DB::table('sale_order_details')->where('sale_order_id', $so->id)->delete();
        foreach ($request->detail as $key => $value) {
            SaleOrderDetail::create(['id' => $value['id'], 'product_id' => $value['product_id'], 'sale_order_id' => $so->id, 'quantity' => $value['quantity']]);
        }

        $request->session()->flash('success', "{$request->submit} โต๊ะ {$so->orderTable->no} สำเร็จ!");

        if ($so->status == 2) {
            $request->session()->flash('print', $so->id);
        }

        if (Auth::user()->role_id == 20) {
            return redirect()->route('public.index');
        }

        return redirect()->route('home.pospos');
    }

    public function cancleOrder(SaleOrders $so, Request $request)
    {
        $so->status = 3;
        $so->save();
        if (Auth::user()->role_id == 20) {
            $request->session()->flash('success', "ยกเลิกโต๊ะ {$so->orderTable->no} สำเร็จ!");
            return redirect()->route('public.index');
        }
    }

    public function printBill(SaleOrders $so)
    {
        $data = fractal($so, new SaleOrderTrans())->toArray()['data'];
        $pdf = PDF::loadView('pdf.bill', ['data' => $data])->setPaper([
            0, 0, 210, 280
                + sizeOf($data['details']) * 20
        ]);
        $outPut = $pdf->output();
        // return view('pdf.bill', ['data' => $data]);
        return response($outPut, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' =>  'inline; filename="' . $so->id . '.pdf"',
        ]);
    }
}
