<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        abort_if(Auth::user()->role_id < 30, 403, 'ท่านไม่ได้รับอนุญาตให้เข้าถึงหน้าดังกล่าว');
        return $next($request);
    }
}
