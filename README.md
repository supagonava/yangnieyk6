<h1>To install this project</h1>
<ul>
    <li>1. composer install</li>
    <li>2. npm install</li>
    <li>3. npm run dev/prod</li>
    <li>4. php artisan migrate</li>
    <li>5. php artisan db:seed</li>
    <li>6. php artisan serve</li>
</ul>
