<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductsCategoriesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\PromotionsController;
use App\Http\Controllers\RawMaterialsController;
use App\Http\Controllers\RawMaterialTypesController;
use App\Http\Controllers\SaleOrdersController;
use App\Http\Controllers\TablesController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name("public.index");
Route::get('/how-to-use', [HomeController::class, 'howToUse'])->name('public.how-to');
Route::get('/logout', [HomeController::class, 'logout'])->name('logout');
Route::put('so/submit', [SaleOrdersController::class, 'submitOrder'])->name('sale-order.submit')->middleware(['auth']);
Route::delete('so/cancle/{so}', [SaleOrdersController::class, 'cancleOrder'])->name('sale-order.cancle')->middleware(['auth']);


Route::middleware(['auth', 'isAdmin'])->prefix('admin')->group(function () {
    Route::get('/', [HomeController::class, 'adminIndex'])->name("home.index");
    Route::get('pospos', [HomeController::class, 'pospos'])->name("home.pospos");
    Route::get('print-bill/{so}', [SaleOrdersController::class, 'printBill'])->name("print-bill");

    Route::post('promotion', [PromotionsController::class, 'store'])->name('promotion.store');
    Route::delete('promotion/{promotion}', [PromotionsController::class, 'destroy'])->name('promotion.destroy');

    Route::resource('user', UserController::class);
    Route::resource('raw-mat', RawMaterialsController::class);
    Route::resource('raw-mat-type', RawMaterialTypesController::class);
    Route::resource('prod-cate', ProductsCategoriesController::class);
    Route::resource('prod', ProductsController::class);
    Route::resource('table', TablesController::class);
    Route::get('table/{table}/toggle', [TablesController::class, 'toggleTable'])->name('table.toggle');
    Route::resource('sale-order', SaleOrdersController::class);
});


Auth::routes();
