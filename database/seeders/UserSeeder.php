<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory(['email' => 'admin@mail.com', 'role_id' => User::ROLE_ADMIN])->create();
        User::factory()->count(10)->create();
    }
}
