<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            RawMaterialTypesSeeder::class,
            RawMaterialsSeeder::class,
            ProductsCategoriesSeeder::class,
            ProductsSeeder::class,
            TablesSeeder::class,
            // SaleOrdersSeeder::class
        ]);
    }
}
