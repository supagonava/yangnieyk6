<?php

namespace Database\Seeders;

use App\Models\Tables;
use Illuminate\Database\Seeder;

class TablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $disable = [
            1 => [9, 10, 11, 12, 13, 14],
            2 => [9, 10, 11, 12, 13, 14],
            3 => [9, 10, 11, 12, 13, 14],
            4 => [9, 10, 11, 12, 13, 14],
            5 => [6, 7, 8, 10, 14],
            6 => [6, 7, 8],
            7 => [6, 7, 8],
        ];
        $count = 1;
        for ($j = 14; $j > 0; $j--) {
            for ($i = 7; $i > 0; $i--) {
                if (!in_array($j, $disable[$i])) {
                    Tables::create(['no' => $count, 'row' => $i, 'col' => $j,]);
                    $count++;
                }
            }
        }
    }
}
