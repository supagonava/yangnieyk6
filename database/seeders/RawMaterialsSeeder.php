<?php

namespace Database\Seeders;

use App\Models\RawMaterials;
use Illuminate\Database\Seeder;

class RawMaterialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RawMaterials::factory()->count(10)->create();
    }
}
