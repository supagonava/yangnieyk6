<?php

namespace Database\Seeders;

use App\Models\RawMaterialTypes;
use Illuminate\Database\Seeder;

class RawMaterialTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RawMaterialTypes::factory()->count(3)->create();
    }
}
