<?php

namespace Database\Seeders;

use App\Models\ProductsCategories;
use Illuminate\Database\Seeder;

class ProductsCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductsCategories::factory()->count(2)->create();
    }
}
