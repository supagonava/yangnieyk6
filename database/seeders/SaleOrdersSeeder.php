<?php

namespace Database\Seeders;

use App\Models\Products;
use App\Models\SaleOrderDetail;
use App\Models\SaleOrders;
use App\Models\Tables;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SaleOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Tables::get() as $table) {
            $saleOrder = SaleOrders::create([
                "table_id" => $table->id,
                "status" => rand(0, 2),
                "amount" => 0,
                "discount" => 0,
                "user_id" => User::where('role_id', "<", 30)->get()->random(1)->first()->id,
                'appointment_at' => Carbon::now()->addHours(5)
            ]);
            for ($i = 0; $i < rand(3, 5); $i++) {
                $prod = Products::get()->random(1)->first();
                $qty = rand(1, 4);
                $saleOrder->amount = $saleOrder->amount + $prod->price * $qty;
                $detail = SaleOrderDetail::create([
                    "sale_order_id" => $saleOrder->id,
                    "product_id" => $prod->id,
                    "quantity" => $qty,
                ]);
            }
            $saleOrder->save();
        }

        for ($x = 0; $x < 500; $x++) {
            $saleOrder = SaleOrders::create([
                "table_id" => 1,
                "status" => 2,
                "amount" => 0,
                "discount" => 0,
                "user_id" => User::where('role_id', "<", 30)->get()->random(1)->first()->id,
                'appointment_at' => Carbon::now()->addHours(5),
                'created_at' => Carbon::now()->subDays(rand(1, 100))
            ]);
            for ($i = 0; $i < rand(3, 5); $i++) {
                $prod = Products::get()->random(1)->first();
                $qty = rand(1, 4);
                $saleOrder->amount = $saleOrder->amount + $prod->price * $qty;
                $detail = SaleOrderDetail::create([
                    "sale_order_id" => $saleOrder->id,
                    "product_id" => $prod->id,
                    "quantity" => $qty,
                ]);
            }
            $saleOrder->save();
        }
    }
}
