<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->integer('stock_quantity')->nullable();
            $table->string('unit')->default('หน่วย');
            $table->integer('point_of_order')->nullable();
            $table->decimal('cost')->nullable();
            $table->decimal('price');
            $table->unsignedBigInteger('updated_via')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('updated_via', 'user_updated_products')->references('id')->on("users")->onUpdate('cascade')->onDelete('set null');
            $table->foreign('category_id', 'product_category')->references('id')->on("products_categories")->onUpdate('cascade')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
