<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('table_id');
            $table->unsignedBigInteger('user_id')->nullable();

            $table->integer('status')->comment('0=>จอง, 1=>กำลังทาน, 2=>ชำระเงินแล้ว');
            $table->integer('amount');
            $table->integer('discount')->default(0)->nullable();
            $table->dateTime('appointment_at')->nullable();

            $table->foreign('table_id', 'sale_order_table')->references('id')->on('tables');
            $table->foreign('user_id', 'sale_order_user')->references('id')->on('tables');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_orders');
    }
}
