<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('raw_materials');
        Schema::create('raw_materials', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->integer('stock_quantity')->default(1);
            $table->string('unit')->default('หน่วย');
            $table->integer('point_of_order')->default(1);
            $table->decimal('cost')->default(0.0)->nullable();
            $table->unsignedBigInteger('updated_via')->nullable();
            $table->unsignedBigInteger('type_id')->nullable();
            $table->foreign('updated_via', 'user_update_stock')->references('id')->on("users")->onUpdate('cascade')->onDelete('set null');
            $table->foreign('type_id', 'raw_mat_with_type')->references('id')->on("raw_material_types")->onUpdate('cascade')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raw_materials');
    }
}
