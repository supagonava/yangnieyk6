<?php

namespace Database\Factories;

use App\Models\SaleOrderDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class SaleOrderDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SaleOrderDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
