<?php

namespace Database\Factories;

use App\Models\RawMaterials;
use App\Models\RawMaterialTypes;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RawMaterialsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RawMaterials::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->text(10),
            "stock_quantity" => $this->faker->numberBetween(100, 200),
            "unit" => $this->faker->text(5),
            "point_of_order" => $this->faker->numberBetween(10, 20),
            "cost" => $this->faker->numberBetween(100, 250),
            "updated_via" => User::get()->random(1)->first()->id,
            "type_id" => RawMaterialTypes::get()->random(1)->first()->id,
        ];
    }
}
