<?php

namespace Database\Factories;

use App\Models\SaleOrders;
use Illuminate\Database\Eloquent\Factories\Factory;

class SaleOrdersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SaleOrders::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
