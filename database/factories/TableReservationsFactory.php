<?php

namespace Database\Factories;

use App\Models\TableReservations;
use Illuminate\Database\Eloquent\Factories\Factory;

class TableReservationsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TableReservations::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
