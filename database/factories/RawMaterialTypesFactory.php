<?php

namespace Database\Factories;

use App\Models\RawMaterialTypes;
use Illuminate\Database\Eloquent\Factories\Factory;

class RawMaterialTypesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RawMaterialTypes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(10),
            'detail' => $this->faker->text(100)
        ];
    }
}
