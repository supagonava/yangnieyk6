<?php

namespace Database\Factories;

use App\Models\Products;
use App\Models\ProductsCategories;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Products::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => "ชุดบุพเฟ่ " . $this->faker->realText(10, 1),
            "stock_quantity" => $this->faker->numberBetween(100, 200),
            "unit" => $this->faker->text(5),
            "point_of_order" => $this->faker->numberBetween(10, 20),
            "cost" => $this->faker->numberBetween(225, 250),
            "price" => $this->faker->numberBetween(250, 300),
            "updated_via" => User::get()->random(1)->first()->id,
            "category_id" => ProductsCategories::get()->random(1)->first()->id,
        ];
    }
}
